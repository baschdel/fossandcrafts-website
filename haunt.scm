;;; fossandcrafts.org website
;;; Copyright © 2016-2020 Christine Lemmer-Webber <cwebber@dustycloud.org>
;;;
;;; Some code borrowed from David Thompson's website
;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; GPLv3 is directly compatible with CC BY-SA 4.0, but for avoidance
;;; of confusion in mixing with website content the following code is
;;; also available under CC BY-SA 4.0 International, as published by
;;; Creative Commons.

(use-modules (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-11)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder rss)
             (haunt builder assets)
             (haunt reader)
             (haunt reader commonmark)
             (web uri))


;;; Utilities
;;; ---------

(define %site-prefix (make-parameter ""))

(define (prefix-url url)
  (string-append (%site-prefix) url))


;;; Templates
;;; ---------

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (alt ,alt))))

(define (header-menu)
  `(("subscribe" ,(prefix-url "/#subscribe"))
    ("archive" ,(prefix-url "/archive/"))
    ("about" ,(prefix-url "/about/"))
    ("contact" ,(prefix-url "/contact/"))
    -
    ("hack & craft" ,(prefix-url "/hack-and-craft/"))))

(define* (base-tmpl site body
                    #:key title #;big-logo)
  `((doctype "html")
    (html
     (head
      (meta (@ (charset "utf-8")))
      (title ,(if title
                  (string-append title " -- FOSS and Crafts")
                  (site-title site)))
      ;; css
      ,(stylesheet "main")
      #;(link (@ (rel "stylesheet")
      (href ,(stylesheet "code.css"))))
      ;; atom feed
      #;(link (@ (rel "alternate")
      (title "FOSS and Crafts")
      (type "application/atom+xml")
      (href ,(prefix-url "/atom-feed.atom"))))
      ;; rss feed
      (link (@ (rel "alternate")
               (title "FOSS and Crafts")
               (type "application/rss+xml")
               (href ,(prefix-url "/rss-feed.rss")))))
     (body
      (div (@ (class "main-wrapper"))
           (header (@ (id "site-header"))
                   ;; Site logo
                   (div (@ (class "header-logo-wrapper"))
                        (a (@ (href ,(prefix-url "/"))
                              (class "header-logo"))
                           ,(base-image "F_and_C_logo_combined-300px.png"
                                        #;(if big-logo
                                        "fossandcrafts-logo-500px.png"
                                        "fossandcrafts-logo-300px.png")
                                        "FOSS and Crafts logo")))
                   ;; Header menu
                   (div (@ #;,(if big-logo
                         '(class "navbar-menu big-navbar")
                         '(class "navbar-menu small-navbar"))
                         (class "navbar-menu"))
                        ,@(map
                           (lambda (item)
                             (match item
                               ((name url)
                                `(div
                                  (a (@ (href ,url))
                                     ,name)))
                               ('- "|")))
                           (header-menu))))
           (div (@ (class "site-main-content"))
                ,body))
      ;; TODO: Link to source.
      (div (@ (class "footer"))
           (a (@ (href "https://gitlab.com/fossandcrafts/fossandcrafts-website"))
              "Site contents")
           " released under "
           (a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
              "Creative Commons Attribution-Sharealike 4.0 International")
           ".  Powered by "
           (a (@ (href "https://dthompson.us/projects/haunt.html"))
              "Haunt")
           ".")))))

(define* (post-template post #:key post-link)
  (define enclosures
    (reverse (post-ref-all post 'enclosure)))
  `(div (@ (class "content-box blogpost"))
        (h1 (@ (class "title"))
            ,(if post-link
                 `(a (@ (href ,post-link))
                     ,(post-ref post 'title))
                 (post-ref post 'title)))
        (div (@ (class "post-about"))
             (span (@ (class "by-line"))
                   ,(post-ref post 'author))
             " -- " ,(date->string* (post-date post)))
        (audio (@ (controls "")
                  (preload "none")
                  (class "post-audio")
                  (style "margin-top: 1em;"))
               ,@(map audio-source enclosures))
        (div (@ (class "post-download"))
             ,@(element-join (map download-button enclosures)))

        (div (@ (class "post-body"))
             ,(post-sxml post))))

(define (post-uri site post)
  (prefix-url
   (string-append "/episodes/" (site-post-slug site post) ".html")))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "episodes-header"))
         (h3 "recent episodes"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define fossandcrafts-haunt-theme
  (theme #:name "FOSS and Crafts"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template))

;; Borrowed from davexunit's blog
(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define* (element-join items #:key [delim " "])
  (let lp ((items items)
           (count 0))
    (match items
      ('() '())
      ((item rest ...)
       (cond
        ((zero? count)
         (cons item (lp rest (1+ count))))
        (else
         (cons delim (cons item (lp rest (1+ count))))))))))

(define (download-button enclosure)
  (define title-text
    (cond
     [(enclosure-title enclosure) =>
      (lambda (title)
        `(": " ,title))]
     [else '("")]))
  `(a (@ (href ,(enclosure-url enclosure)))
      "[Download" ,@title-text "]"))

(define (audio-source enclosure)
  `(source (@ (src ,(enclosure-url enclosure))
              (type ,(enclosure-mime-type enclosure)))))

(define (post-preview post site)
  `(li (a (@ (href ,(post-uri site post)))
          ,(post-ref post 'title))
       (div (@ (class "news-feed-content"))
            (div (@ (class "news-feed-item-date"))
                 ,(date->string* (post-date post)))
            ,(first-paragraph post)
            (div (@ (class "consume-more-buttons"))
                 ,@(element-join
                    (cons
                     `(a (@ (href ,(post-uri site post)))
                         "[Read more ==>]")
                     (map download-button
                          (reverse
                           (post-ref-all post 'enclosure)))))))))


;;; Pages
(define (index-content site posts)
  `(div
    ;; Main intro
    (div (@ (class "content-box bigger-text")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 "A podcast about free software, free culture, "
             "and making things together")
         (p "FOSS and Crafts is an interdisciplinary exploration of "
            "collaborative creation.  We believe that all works (whether "
            "digital or traditional in nature) are informed by history as well as "
            "one's surrounding culture and community, whether directly or "
            "indirectly, and that everyone benefits from the expansion of the commons.  "
            "We explore such topics as computer programming, "
            "craft production, user freedom and agency, especially informed "
            "by the principles of the F(L)OSS (Free, Libre, and Open Source Software) "
            "and free culture movements.")
         (p "FOSS and Crafts's usual hosts are "
            (a (@ (href "https://mlemmer.org/"))
               "Morgan Lemmer-Webber")
            " and "
            (a (@ (href "https://dustycloud.org/"))
               "Christine Lemmer-Webber") ".")
         (h2 (@ (id "subscribe"))
             "How do I listen?")
         (p "Subscribe with your favorite podcatcher via:")
         (ul (li (b "RSS:")
                 " "
                 (a (@ (href ,(prefix-url "/rss-feed.rss")))
                    "[mp3]")
                 " "
                 (a (@ (href ,(prefix-url "/rss-feed-ogg.rss")))
                    "[ogg vorbis]"))
             (li (b "Atom:")
                 " "
                 (a (@ (href ,(prefix-url "/atom-feed.atom")))
                    "[mp3 + ogg vorbis]")))
         (p "Don't have a favorite podcatcher?  We like "
            (a (@ (href "https://antennapod.org/"))
               "Antennapod") ".")

         (hr (@ (class "pre-follow-us")))
         (p (@ (class "follow-us"))
            (span (@ (style "padding-right: 1.5em;"))
                  (b "Follow us: ")
                  (a (@ (href "https://octodon.social/@fossandcrafts"))
                     "[fediverse]")
                  " "
                  (a (@ (href "https://twitter.com/fossandcrafts"))
                     "[twitter]"))
            " ~ ♥ ~ "
            (span (@ (style "padding-left: 1.5em;"))
                  (b "Donate: ")
                  (a (@ (href "https://www.patreon.com/fossandcrafts"))
                     "[Patreon]")
                  " "
                  (a (@ (href "https://liberapay.com/cwebber"))
                     "[Liberapay]"))))

    ;; News updates and etc
    (div (@ (class "content-box homepage-news-box")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 (@ (class "title"))
             "Episodes and news")
         (ul (@ (class "homepage-news-items"))
             ,@(map (lambda (post)
                      (post-preview post site))
                    (take-up-to 10 (posts/reverse-chronological posts))))
         (p (@ (style "text-align: center"))
            (a (@ (href "/archive/"))
               "[--archive--]")))))

(define (archive-content site posts)
  `(div (@ (class "content-box bigger-text")
	   (style "margin-top: 20px; margin-bottom: 20px;"))
        (h1 (@ (class "title"))
	    "The Archive")
        (ul (@ (class "homepage-news-items"))
            ,@(map (lambda (post)
                     (post-preview post site))
                   (posts/reverse-chronological posts)))))

(define (about-content)
  '(div
    (div (@ (class "content-box bigger-text")
	    (style "margin-top: 20px; margin-bottom: 20px;"))
	 (h1 "About FOSS and Crafts")

         (p "FOSS and Crafts was conceived as an overly ambitious plan to unite the "
            "co-host's interests of study while saying \"gosh, aren't all these "
            "problems related\", and nodding confidently to themselves that all "
            "these themes would fit nicely together and everyone would be interested "
            "in them.")

         (p "If you like this podcast, you could "
            (a (@ (href "https://www.patreon.com/patreon"))
               "donate to the FOSS and Crafts Studios Patreon account")
            "!")

         (h2 "About the hosts")

         (div (@ (class "host-profile"))
              (a (@ (href "https://mlemmer.org/"))
                 (img (@ (class "teacup-avatar")
                         (src "/static/images/teacup-avatar.png")
                         (alt "Embroidered teacup"))))
              (p (b "Morgan Lemmer-Webber") " "
                 "is an art historian, avid crafter, and FOSS user and advocate. "
                 "She has a PhD in "
                 (a (@ (href "https://arthistory.wisc.edu/"))
                    "Art History")
                 " from the "
                 (a (@ (href "https://wisc.edu"))
                    "University of Wisconsin, Madison") ". "
                 "A lifelong exploration of various handcrafts and media inspired "
                 "her dissertation research on women and textile production in the "
                 "Roman empire. "
                 "Morgan is also interested in the intersection of FOSS and academia, "
                 "particularly in the context of digital humanities research. "
                 "She built the original "
                 (a (@ (href "https://ramsay.arthistory.wisc.edu/"))
                    "digitization of the William Ramsay ledger")
                 " as a custom static site generator in Python. "
                 "She looks forward to doing more digital humanities exploration as "
                 "soon as her PhD dissertation is out the door."))

         (div (@ (class "host-profile"))
              (a (@ (href "https://dustycloud.org/"))
                 (img (@ (class "teacup-avatar")
                         (src "/static/images/mug-avatar.png")
                         (alt "Embroidered mug"))))
              (p (b "Christine Lemmer-Webber") " "
                 "is a long-time user freedom advocate. "
                 "Her degree in interdisciplinary humanities with a focus "
                 "on philosophy and ethics has informed her approach to free "
                 "and open source software and free culture. She is mostly "
                 "known for her work co-authoring and co-editing the "
                 (a (@ (href "https://www.w3.org/TR/activitypub/"))
                    "ActivityPub") " "
                 "distributed social network protocol. In previous times of her "
                 "life she worked as tech lead at "
                 (a (@ (href "https://creativecommons.org/"))
                    "Creative Commons")
                 ", co-founded "
                 (a (@ (href "https://mediagoblin.org/"))
                    "MediaGoblin")", "
                 "started and ran the "
                 (a (@ (href "https://lpc.opengameart.org/"))
                    "Liberated Pixel Cup")", "
                 "and kicked off the work on "
                 (a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
                    "CC BY-SA 4.0") " and "
                 (a (@ (href "https://www.gnu.org/licenses/gpl-3.0.en.html"))
                    "GPL") " "
                 "compatibility. These days her primary work is on "
                 (a (@ (href "https://gitlab.com/spritely/"))
                    "Spritely") ", "
                 "a project to improve the security "
                 "of federated social networks and bridge them with virtual worlds. "
                 "When she isn't programming, she enjoys cooking, sketching, and "
                 "making ASCII art. ")))))

(define (contact-content)
  '(div
    (div (@ (class "content-box bigger-text")
	    (style "margin-top: 20px; margin-bottom: 20px;"))
	 (h1 (@ (class "title"))
	     "Contact Us")
	 (p "You can contact the FOSS and Crafts crew a number of ways!")
         (ul
	  (li "Email us at podcast at fossandcrafts.org")
	  (li "Find us on IRC at #fossandcrafts on "
              (a (@ (href "https://libera.chat"))
                 "libera.chat")
              " ("
              (a (@ (href "https://libera.chat/guides/connect"))
                 "how to connect")
              ", "
              (a (@ (href "https://libera.chat/guides/clients"))
                 "choosing a client")")")
	  (li "We're " (a (@ (href "https://octodon.social/@fossandcrafts"))
                          "on the fediverse"))
	  (li "We're on Twitter at " (a (@ (href "https://twitter.com/fossandcrafts"))
                                        "@fossandcrafts"))))))


(define (handc-content)
  `(div
    (div (@ (class "content-box bigger-text")
	    (style "margin-top: 20px; margin-bottom: 20px;"))
         (img (@ (class "handc-logo")
                 (src "/static/images/handc-wip-crop-scaled-small.jpg")
                 (alt "Hack & Crafts! logo: ascii art of a house, embroidered onto cloth")))

	 (h1 "Hack and Craft!")

         (p "Hack and Craft is a virtual meeting that is something between a user group and a stitch and bitch. In keeping with the themes of our podcast, FOSS & Crafts, we are creating a space where we can talk about free software and free culture while we make things together. Bring your latest project to work on as we get to know each other.")
         
         (p "By joining the Big Blue Button room for Hack and Craft, you agree to adhere to our "
            (a (@ (href "https://www.contributor-covenant.org/version/2/0/code_of_conduct/"))
               "Code of Conduct."))

         (p (b "When: ")
            "Morgan and Christine are based in ET, but we want this thing to be "
            "possible to attend from an international audience.  So we do it "
            "twice a month at two different times! "
            "(Converting to your time zone is left as an exercise for the "
            "reader/attendee.)"
            (ul (li "Every " (b "first") " Saturday of the month at " (b "20:00-22:00 ET"))
                (li "Every " (b "third") " Saturday of the month at " (b "14:00-16:00 ET"))))

         (p (b "Location: ")
            (a (@ (href "https://bbb.sfconservancy.org/b/chr-4fm-amb-air"))
               "Hack & Craft Big Blue Button room")
            " (thank you to "
            (a (@ (href "https://sfconservancy.org/"))
               "Software Freedom Conservancy")
            " for letting us use their Big Blue Button instance!)"))))

(define (index-page site posts)
  (make-page
   "index.html"
   (base-tmpl site
              (index-content site posts)
              ;; #:big-logo #t
              )
   sxml->html))

(define (about-page site posts)
  (make-page
   "/about/index.html"
   (base-tmpl site
	      (about-content)
              #:title "About F&C!")
   sxml->html))

(define (contact-page site posts)
  (make-page
   "/contact/index.html"
   (base-tmpl site
	      (contact-content)
              #:title "Contact Us!")
   sxml->html))

(define (archive-page site posts)
  (make-page
   "/archive/index.html"
   (base-tmpl site
	      (archive-content site posts)
              #:title "Archive")
   sxml->html))

(define (handc-page site posts)
  (make-page
   "/hack-and-craft/index.html"
   (base-tmpl site
	      (handc-content)
              #:title "Hack & Craft")
   sxml->html))



;;; RSS feed transformations for itunes
(define* (adjust-rss-feed author summary owner-email image
                          #:key (file-name "feed.xml")
                          (owner-name author)
                          (explicit? #f)
                          (itunes-categories '()))
  (lambda (feed site posts)
    (define base-uri
      (build-uri (site-scheme site)
                 #:host (site-domain site)))
    (define (add-atom-ns feed)
      (sxml-acons feed 'xmlns:itunes "http://www.itunes.com/dtds/podcast-1.0.dtd"))
    (define (add-itunes-ns feed)
      (sxml-acons feed 'xmlns:atom "http://www.w3.org/2005/Atom"))
    (define (channel+ feed item)
      (match feed
        [('rss ('@ feed-attribs ...)
               channel)
         `(rss (@ ,@feed-attribs)
               ,(sxml-cons channel item))]))
    (define (add-language-tag feed)
      (channel+ feed '(language "en-us")))
    (define (add-copyright feed)
      (channel+ feed  '(copyright "Licensed under CC BY-SA 4.0 International")))
    (define (add-atom-linkhref feed)
      (channel+ feed
                `(atom:link
                  (@ (href ,(uri->string
                             (add-path-to-uri base-uri
                                              file-name)))
                     (rel "self")))))
    (define (add-itunes-author feed)
      (channel+ feed `(itunes:author ,author)))
    (define (add-itunes-owner feed)
      (channel+ feed `(itunes:owner
                       (itunes:name ,owner-name)
                       (itunes:email ,owner-email))))
    (define (add-itunes-summary feed)
      (channel+ feed `(itunes:summary ,summary)))
    (define (add-itunes-explicit feed)
      (channel+ feed
                (if explicit?
                    '(itunes:explicit "yes")
                    '(itunes:explicit "no"))))
    (define (add-itunes-image feed)
      (channel+ feed
                `(itunes:image (@ (href ,image)))))
    (define (add-itunes-categories feed)
      (fold (lambda (category prev)
              (match category
                [(? string? category)
                 (channel+ prev
                           `(itunes:category (@ (text ,category))))]
                [((? string? category)
                  (subcategories ...))
                 (let ([subcategories-sxml
                        (map (lambda (sc)
                               `(itunes:category (@ (text ,sc))))
                             subcategories)])
                   (channel+ prev
                             `(itunes:category (@ (text ,category))
                                               ,@subcategories-sxml)))]))
            feed itunes-categories))
    (define (add-author feed)
      (channel+ feed
                `(author
                  ,(string-append (if owner-email
                                      (string-append owner-email " ")
                                      "")
                                  (if author
                                      (string-append "(" author ")")
                                      "")))))
    (define (add-image feed)
      (channel+ feed
                `(image (url ,image)
                        (title "FOSS and Crafts")
                        (link ,(uri->string base-uri)))))
    (define munge-it
      (compose add-atom-ns add-itunes-ns add-language-tag
               add-atom-linkhref add-copyright
               add-itunes-author add-itunes-owner add-itunes-summary
               add-itunes-explicit add-itunes-image add-itunes-categories
               add-author add-image))
    (munge-it feed)))

(define* (adjust-rss-item image
                          #:key [explicit? #t])
  (lambda (item site post)
    (define (add-itunes-image item)
      (sxml-cons item
                 `(itunes:image (@ (href ,image)))))
    (define (add-itunes-explicit item)
      (sxml-cons item
                 (if explicit?
                    '(itunes:explicit "yes")
                    '(itunes:explicit "no"))))
    (define (add-guid item)
      (sxml-cons item
                 `(guid ,(or (post-ref post 'guid)
                             (post-slug post)))))
    (define munge-it
      (compose add-itunes-image add-itunes-explicit add-guid))
    (munge-it item)))

(define (adjust-rss-item-filter-enclosures enclosure-type)
  (lambda (item site post)
    (fold-right
     (match-lambda*
       [(('enclosure (@ attrs ...)) prev)
        (if (equal? (assoc 'type attrs)
                    `(type ,enclosure-type))
            (cons `(enclosure (@ ,@attrs))
                  prev)
            prev)]
       [(sub-item prev) (cons sub-item prev)])
     '()
     item)))

(define (adjust-rss-enclosure-duration sxml site post enclosure)
  ;; Add the itunes duration property
  (match (assoc 'duration (enclosure-extra enclosure))
    [(_ . val)
     (sxml-acons sxml 'itunes:duration val)]
    [#f sxml]))



;;; Site

(define fandc-subtitle
  "A podcast about free software, free culture, and making things together.")
(define fandc-logo-url
  "https://fossandcrafts.org/static/images/F_and_C_logo_combined.jpg")

(define max-entries 1024)

(define* (make-rss-feed file-name
                        #:optional [enclosure-type #f])
  (rss-feed #:blog-prefix "/episodes"
            #:file-name file-name
            #:subtitle fandc-subtitle
            #:max-entries max-entries
            #:adjust-feed
            (list
             (adjust-rss-feed
              "FOSS and Crafts"
              fandc-subtitle "podcast@fossandcrafts.org"
              fandc-logo-url
              #:explicit? #t
              #:itunes-categories '("Technology"
                                    ("Leisure"
                                     ("Crafts")))))
            #:adjust-item
            (if enclosure-type
                (list (adjust-rss-item fandc-logo-url)
                      (adjust-rss-item-filter-enclosures enclosure-type))
                (list (adjust-rss-item fandc-logo-url)))
            #:adjust-enclosure
            (list adjust-rss-enclosure-duration)))

(site #:title "FOSS and Crafts"
      #:domain "fossandcrafts.org"
      #:scheme 'https
      #:default-metadata
      '((author . "FOSS and Crafts")
        (email . "podcast@fossandcrafts.org"))
      #:readers (list commonmark-reader)
      #:builders (list (blog #:prefix "/episodes"
                             #:theme fossandcrafts-haunt-theme)
                       index-page
                       (atom-feed #:blog-prefix "/episodes"
                                  #:max-entries max-entries
                                  #:file-name "atom-feed.atom")
                       ;; We used to have a feed at this uri, so render a duplicate
                       ;; atom feed here for backwards compatibility
                       (atom-feed #:blog-prefix "/episodes"
                                  #:file-name "atom-feed.xml"
                                  #:max-entries max-entries
                                  #:subtitle fandc-subtitle)
                       (make-rss-feed "rss-feed.rss" 'audio/mpeg)
                       (make-rss-feed "rss-feed-ogg.rss" 'audio/ogg)
                       (static-directory "static" "static")
		       about-page
		       contact-page
		       archive-page
		       handc-page
                       (atom-feeds-by-tag)))
