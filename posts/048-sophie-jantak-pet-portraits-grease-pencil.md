title: 48: Sophie Jantak on pet portraits and Blender's Grease Pencil
date: 2022-06-30 13:40
tags: foss, crafts, blender, pets
slug: 48-sophie-jantak-pet-portraits-grease-pencil
summary: Sophie Jantak talks about how she uses Blender's Grease Pencil to do pet portraits
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/048-sophie-jantak-pet-portraits-grease-pencil.ogg" length:"00:29:36" duration:"22879959"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/048-sophie-jantak-pet-portraits-grease-pencil.mp3" length:"00:29:36" duration:"28421209"
---
[![Sophie Jantak's fabulous portrait of our cats](/static/images/blog/sophie-final2-scaled.png)](https://dustycloud.org/gfx/goodies/sophie-jantak-anniversary-cats-big.png)

The amazing [Sophie Jantak](https://www.sophiejantak.com/) joins us to
talk about how she makes pet portraits (including one she made for us!)
using [Blender's](https://www.blender.org/)
[Grease Pencil](https://docs.blender.org/manual/en/latest/grease_pencil/index.html).
Hear about Sophie's process, why Grease Pencil is the right tool for her,
and what her collalboration process is like on pet portrait commissions!
(And yes, you can [commission Sophie tool](https://www.sophiejantak.com/pet-commissions)!)

**BONUS FREE CULTURAL SOURCE CONTENT!**
We've collectively decided to release this artwork's source
code as a free cultural work!
[Get the .blend](https://dustycloud.org/gfx/goodies/sophie-jantak-anniversary-cats.blend) ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/))!

**HACK AND CRAFT SCHEME TUTORIALS!**
Also a reminder, we'll be hosting two versions of a "Intro to Scheme"
tutorial during the two [Hack & Craft](https://fossandcrafts.org/hack-and-craft/)
meetings this month!

 - **July 2nd, 8pm-10pm ET (12am-2am UTC):** First trial run of Scheme tutorial!
 - **July 16, 2pm-4pm ET (6pm-8pm UTC):** Second version, we're planning to record
   this one!

**Links:**

 - [Sophie Jantak](https://www.sophiejantak.com/)!

   - [YouTube channel](https://www.youtube.com/c/SophieJantak) (lots of great grease pencil tutorials!)

   - [Pet commissions](https://www.sophiejantak.com/pet-commissions)

   - [Patreon](https://www.patreon.com/sophiejantak)

   - [Sophie's beginner grease pencil tutorial: 3d bonsai painting](https://www.youtube.com/watch?v=NA3wZBC1Nvw)

 - [Blender](https://www.blender.org/) and [Grease Pencil](https://docs.blender.org/manual/en/latest/grease_pencil/index.html) ([hybrid 2d and 3d artwork](https://www.blender.org/features/story-artist/))

 - Christine's cat comix (these were made for Morgan when she was
   finishing her dissertation, but maybe you'll enjoy them):

   - [1: Deadlines](https://dustycloud.org/gfx/goodies/cat-comic-1-deadlines.png)

   - [2: The Anxiety Cloud](https://dustycloud.org/gfx/goodies/cat-comic-2-anxiety-cloud.png)

   - [3: Missy's Adventures in Video Gaming](https://dustycloud.org/gfx/goodies/cat-comic-3-video-games.png)

   - [4: Missy's NES cart](https://dustycloud.org/gfx/goodies/cat-comic-4-nes-cart.png)

   - [5: Enter Kelsey the Queen](https://dustycloud.org/gfx/goodies/cat-comic-5-kelsey-ghost.png)

   - [6: Kelsey Claims the House for Herself](https://dustycloud.org/gfx/goodies/cat-comic-6-kelsey-claims-the-house.png)

   - [7: Missy's Revenge](https://dustycloud.org/gfx/goodies/cat-comic-7-missys-revenge.png)

   - [8: Kelsey's Demand](https://dustycloud.org/gfx/goodies/cat-comic-8-kelseys-demand.png)

 - [HERO](https://www.youtube.com/watch?v=pKmSdY56VtY), a Blender Grease Pencil Showcase

 - There are a lot of good Grease Pencil tutorials online...
   we'll let you find them, but this
   [Grease Pencil Random Tips and Tricks](https://www.youtube.com/watch?v=VkOovs_90V8&list=PLvashLL2utJGz0yuPBU7gGn-smNT8A3JM)
   is a nice thing to know about!

 - [FOSS & Crafts Episode 16: Bassam Kurdali on using Blender for open movie productions and education](https://fossandcrafts.org/episodes/16-bassam-kurdali-blender-open-movies-education.html)
