title: 41: Learning Emacs
date: 2022-02-05 12:40
tags: foss, emacs
slug: 41-learning-emacs
summary: Morgan finally overcomes her fear of Emacs and we talk about our respective experiences learning it
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/041-learning-emacs.ogg" length:"43218849" duration:"00:50:32"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/041-learning-emacs.mp3" length:"48527510" duration:"00:50:32"
---
Morgan finally overcomes her fear of [Emacs](https://www.gnu.org/software/emacs/)
and we talk about Morgan and Christine's respective experiences
learning it, and how you can pick it up too!

**Our talks tomorrow** at [FOSDEM](https://fosdem.org)'s
[Declarative and Minimalistic Computing](https://fosdem.org/2022/schedule/track/declarative_and_minimalistic_computing/)
room:

- [Lisp but Beautiful; Lisp for Everyone](https://fosdem.org/2022/schedule/event/lispforeveryone/)
- [Spritely Goblins Comes to Guile](https://fosdem.org/2022/schedule/event/spritelygoblins/)

**Switching capslock and ctrl stuff:** (it's a great idea even if you don't
use Emacs; [many keyboards used to have ctrl key where capslock now is](http://xahlee.info/kbd/keyboard_ctrl_vs_capslock_position.html),
and much advanced program use benefits from keyboard shortcuts):

 - [Switching on GNU/Linux](https://opensource.com/article/18/11/how-swap-ctrl-and-caps-lock-your-keyboard)

 - [Switching on Windows 10 & 11](https://zachrussell.net/blog/map-caps-lock-to-control-windows/)

 - [Switching on OSX](https://www.popularmechanics.com/technology/a25709/caps-lock-to-backspace-control/)

 - On [Guix](https://guix.gnu.org/): `(keyboard-layout "us" #:options '("caps:ctrl_modifier" "shift:both_capslock"))` in your system configuration both makes capslock a ctrl and allows you to press both shift keys at once to enable capslock behavior (should you want such a thing)

 - And actually there's [a whole EmacsWiki page about it](https://www.emacswiki.org/emacs/MovingTheCtrlKey)

**Links:**

 - [Emacs](https://www.gnu.org/software/emacs/)!

 - [Git](https://git-scm.com/) and [Magit](https://magit.vc/)

 - [Org-Mode](https://orgmode.org/)

 - [Spacemacs](https://www.spacemacs.org/)

 - [Mousemacs](https://github.com/corvideon/mousemacs)

 - [Emacs Themes](https://emacsthemes.com/)&#x2026; find one that's right for you!

 - [The Emacs lisp reference manual](https://www.gnu.org/software/emacs/manual/elisp.html)

 - [emacsconf](https://emacsconf.org/)

 - [Org-mode and Org-Roam for Scholars and Researchers](https://emacsconf.org/2020/talks/17/)

 - [Sacha Chua](https://sachachua.com/), whose blog is full of awesome [emacs](https://sachachua.com/blog/category/emacs) and [emacs news](https://sachachua.com/blog/category/emacs-news)
   posts, and who also releases [lots of great videos about Emacs](https://www.youtube.com/c/SachaChua)!

 - [Emacs Rocks!](https://www.youtube.com/user/emacsrocks)

 - [Episode 14: Digital Humanities Workshops](https://fossandcrafts.org/episodes/14-digital-humanities-workshops.html)

 - [Episode 15: Scribble and the Open Document Format](https://fossandcrafts.org/episodes/15-scribble-and-the-open-document-format.html)

 - [mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html), [ERC](https://www.gnu.org/software/emacs/manual/html_mono/erc.html), [crdt.el](https://code.librehq.com/qhong/crdt.el) ([video](https://www.youtube.com/watch?v=JYv3QF_-QcU))&#x2026; many more emacs tools mentioned,
   not all linked!  Trying to be comprehensive would result in a
   trip to the `M-x doctor` for sure&#x2026;

 - [wireworld-el](https://gitlab.com/dustyweb/wireworld-el), Christine's (minimalist) implementation of the
   [wireworld cellular automata](https://www.quinapalus.com/wi-index.html) (cellular automata circuits!)

 - And yes, it turns out you CAN annotate PDFs in emacs, using
   the [pdf-tools](https://github.com/politza/pdf-tools) package!
