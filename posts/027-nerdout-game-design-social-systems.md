title: 27: Nerdout! Game Design and Social Systems
date: 2021-05-06 14:12
tags: nerdout, games, game design, social systems, society, rules
slug: 27-nerdout-game-design-social-systems
summary: Steve and Chris talk about social systems viewed through the lens of game design
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/027-game-design-social-systems.ogg" length:"45952123" duration:"01:17:41"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/027-game-design-social-systems.mp3" length:"74578160" duration:"01:17:41"
---
Steve is back, talking with Chris about viewing social systems through
the lens of game design.
How do game mechanics, uncertainty, and narrative map onto governance,
society, and citizen participation?

Thanks to Kate and Ricky for participating in a pre-show discussion
which generated many of the ideas explored in this episode.

**Links:**

 - The first [F&C Nerdout episode on "Fuzzy and crisp systems"](/episodes/23-nerdout-fuzzy-and-crisp.html),
   in case you wanted to hear more of the Chris & Steve conversation dynamic

 - [F&C Episode 4: The Eight Kinds of Fun](/episodes/4-the-eight-kinds-of-fun.html), and the paper it was inspired from, [MDA: A Formal Approach to Game Design and Game Research](https://fossandcrafts.org/episodes/4-the-eight-kinds-of-fun.html)

 - [Veil of Ignorance](https://en.wikipedia.org/wiki/Original_position)
   thought experiment by philosopher John Rawls

 - [Comeback Mechanic](https://www.giantbomb.com/comeback-mechanic/3015-7243/)

 - [Power Grid](https://en.wikipedia.org/wiki/Power_Grid)

 - The [Ludology](https://ludology.libsyn.com/) podcast... if you like
   this episode, you'll probably like Ludology.

   - [Ludology does a "Biography of a Board Game" on Candyland](https://ludology.libsyn.com/gametek-classic-167-candyland)

   - [Ludology Episode 3 - Catch the Leader](https://ludology.libsyn.com/episode-3-catch-the-leader) which includes the first critique of Power Grid

   - [Ludology Episode 3A - The Defense of Power Grid](https://ludology.libsyn.com/episode-3a-the-defense-of-power-grid)

 - [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

 - [Emergence](https://en.wikipedia.org/wiki/Emergence) (the broad
   umbrella which includes "emergent behavior")

 - [How cats get drunk in Dwarf Fortress, and why its creators haven't figured out time travel (yet)](https://www.pcgamer.com/how-cats-get-drunk-in-dwarf-fortress-and-why-its-creators-havent-figured-out-time-travel-yet/) (Note that we cut off the story before it got sad in the episode... the following link tells the full story, including the sad end to this particular form of emergent behavior)

 - [Game Analysis: Love Letter](https://drdisc101.wordpress.com/2016/03/09/game-analysis-love-letter/)

 - Apparently the game about the soldiers gaining PTSD and getting drunk was called "Clockwork Empires" (note, another proprietary game and we haven't played it; it's an empire-colonialist-expansion game, but maybe a bit more consciously so... we don't know really though).  This is the [closest article we could find about the behavior](https://www.gamespot.com/amp-articles/getting-drunk-and-getting-high-in-clockwork-empires/1100-6418440/) but it doesn't seem to be the full interview Steve remembered.  Note that Steve had a followup email saying: "Also, a detail I apparently forgot is that the alcohol helped them forget (temporarily?) their lost friends, which feels important..."
