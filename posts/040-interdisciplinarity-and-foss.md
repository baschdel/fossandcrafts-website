title: 40: Interdisciplinarity and FOSS (SeaGL Keynote)
date: 2022-01-09 23:00
tags: foss, crafts
slug: 40-interdisciplinarity-and-foss
summary: Audio from Morgan and Christine's SeaGL keynote about how interdisciplinary approaches can help FOSS projects
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/040-interdisciplinarity-in-foss.ogg" length:"16945965" duration:"00:25:44"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/040-interdisciplinarity-in-foss.mp3" length:"24721001" duration:"00:25:44"
---
Morgan and Christine talk about the skills they’ve learned in their
humanities backgrounds and how those have translated into their work
within FOSS communities and projects. They’ll then discuss the
benefits of seeking out varied skillsets within your communities, the
value of looking at problems from multiple lenses, and how to use all
of the tools we’ve got to promote our projects.
(This episode is the audio from
[our SeaGL keynote of the same name](https://seagl.org/archive/2021/interdisciplinarity-in-foss)!)

Oh yeah, and as we said in the intro, the
[TinyNES campaign is going strong](https://www.crowdsupply.com/tall-dog-electronics/tinynes/) (see our [last episode](https://fossandcrafts.org/episodes/39-tinynes.html))!
We met the minimum goal which means it's happening!
Still a couple of weeks left (at time of writing) to get yourself
an open hardware NES, but over half of the "genuine chip" ones are
now sold out, so get yours while you can!

**Links:**

 - [SeaGL](https://seagl.org/), and a
   [video of our keynote of the same name](https://seagl.org/archive/2021/interdisciplinarity-in-foss)

 - [Spritely](https://spritelyproject.org/)
 
 - Okay here's Christine's old comic [Lingo](http://lingocomic.com/)
   from nearly two decades ago, don't judge too harshly pls
   
 - [Emacs](https://www.gnu.org/software/emacs/)

 - [LaTeX](https://www.latex-project.org/)
 
 - [Racket](https://racket-lang.org/) and
   [Racket's picture language](https://docs.racket-lang.org/pict/)

 - [MediaGoblin](https://mediagoblin.org/)
 
 - [Digital Humanities Workshops episode](https://fossandcrafts.org/episodes/14-digital-humanities-workshops.html)
 
 - [Music Production on Guix System](https://guix.gnu.org/en/blog/2020/music-production-on-guix-system/)
 
 - [Jessica Tallon and Spritely interview](https://spritelyproject.org/news/interview-with-jessica-tallon.html)

 - [FOSS Stitch episode of F&C](https://fossandcrafts.org/episodes/28-foss-stitch-with-ehashman-glasnt.html), and [ih!](https://github.com/glasnt/ih)

 - [The Liberated Pixel Cup](https://lpc.opengameart.org/)
 
 - [Deb Nicholson](https://twitter.com/baconandcoconut)
 
 - [Urchn Studios](https://urchn.org/)
