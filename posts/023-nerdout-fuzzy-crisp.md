title: 23: Nerdout! Fuzzy and crisp systems
date: 2021-02-13 12:15
tags: foss, fuzzy, crisp, lisp, lojban, nerdout, steve
slug: 23-nerdout-fuzzy-and-crisp
summary: Steve Webber joins as a guest co-host to talk about fuzzy and crisp systems
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/023-nerdout-fuzzy-and-crisp.ogg" length:"27795593" duration:"00:44:52"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/023-nerdout-fuzzy-and-crisp.mp3" length:"43079017" duration:"00:44:52"
---
Morgan is in the final crunch of finishing her dissertation draft, so
Chris's brother Steve Webber joins us for a special "nerdout":
analyzing the dual nature of fuzzy vs crisp systems!  From physics to
biology, from programming languages to human languages, the duality of
fuzzy and crisp is everpresent.

Yes, this really is what Chris and Steve sound like whenever they get
together...

**Links:**

 - [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html)
   (but [this version looks better on the web](https://sarabander.github.io/sicp/))
   and the [1980s lectures](https://www.youtube.com/watch?v=-J_xL4IGhJA&list=PLE18841CABEA24090)
   (also on [Internet Archive](https://archive.org/details/SICP_4_ipod)
   but the YouTube uploads are more recent and higher quality)

 - [Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language)) and [Scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language))

 - [Lambda Calculus](https://en.wikipedia.org/wiki/Lambda_calculus)
 
 - [The Little Schemer](https://mitpress.mit.edu/books/little-schemer-fourth-edition)

 - [The Most Beautiful Program Ever Written](https://www.youtube.com/watch?v=OyfBQmvr2Hc) by William Byrd

 - [Lisp 1.5 programmer's manual](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf), which also now has a lovely [reprint for sale](https://mitpress.mit.edu/books/lisp-15-programmers-manual) (see Appendix B for Lisp in Lisp, albeit in m-expression rather than s-expression format... m-expressions never took on)
 
 - [Javascript: The Good Parts](https://www.oreilly.com/library/view/javascript-the-good/9780596517748/)

 - [The narcissism of small differences](https://en.wikipedia.org/wiki/Narcissism_of_small_differences)

 - [To Mock a Mockingbird](https://en.wikipedia.org/wiki/To_Mock_a_Mockingbird)
   by Raymond Smullyan.  Also, presumably not the link Steve had shared with Chris
   back in the day (but maybe it was?) but here's a more math'y breakdown of
   some of the ideas, [To Dissect a Mockingbird: A Graphical Notation for the Lambda Calculus with Animated Reduction](https://dkeenan.com/Lambda/)
   
 - [Duality (mathematics)](https://en.wikipedia.org/wiki/Duality_(mathematics))

 - [Fuzzy and crisp sets](https://techdifferences.com/difference-between-fuzzy-set-and-crisp-set.html)

 - [Neats and scruffies](https://en.wikipedia.org/wiki/Neats_and_scruffies)
   (see also our [previous episode about machine learning](https://fossandcrafts.org/episodes/2-machine-learning-impact.html))

 - [Alan Watts' lecture on "prickles and goo"](https://www.youtube.com/watch?v=D4vHnM8WPvU)

 - [Carcinisation](https://en.wikipedia.org/wiki/Carcinisation)
   (convergent evolution on "crabs")

 - [Lisp vs APL: "Mud and Diamonds"](https://wiki.c2.com/?JoelMosesOnAplAndLisp)

 - [Guix](http://guix.gnu.org/)

 - [Jonathan Rees's website](http://mumble.net/~jar/)

 - [Lojban](https://en.wikipedia.org/wiki/Lojban),
   and here's a pretty good [Lojban intro](https://mw.lojban.org/papri/la_karda)

 - The infamous Lojban ["bear goo"](https://mw.lojban.org/papri/jbocre:_Bear_goo)
   debate
