title: 45: A high level introduction to cryptography
date: 2022-05-25 11:30
tags: foss, cryptography
slug: 45-intro-to-cryptography
summary: In this episode we give a very high level introduction to cryptography concepts.
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/045-intro-to-cryptography.ogg" length:"00:33:28" duration:"28878588"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/045-intro-to-cryptography.mp3" length:"00:33:28" duration:"32138103"
---
In this episode we give a very (very) high level introduction to
cryptography concepts.  No math or programming background required!

**Links:**

- [Crypto 101](https://www.crypto101.io/),
  probably the BEST book for learning about cryptography concepts.
  And a [relevant talk from PyCon](https://www.youtube.com/watch?v=3rmCGsCYJF8)!

- We mentioned
  [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)),
  which is the first publicly published algorithm for public key
  cryptography.  These days most public key cryptography uses
  [elliptic curves](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography)
  instead.  It's possible that in
  the future, something else will be recommended instead!

- Playing around with [GnuPG](https://gnupg.org/) can be a great way to learn about
  cryptography as a user, but... it's also not the easiest thing to
  learn either, and we don't personally believe that GPG/PGP's web of
  trust model is a realistic path for user security.  (But what we
  recommend instead, that's a topic for a future episode.)  Still,
  a useful tool in all sorts of ways.

- Mixing and matching these things at a low level can be tricky,
  and unexpected vulnerabilities can easily occur.
  [Cryptographic Right Answers](https://latacora.micro.blog/2018/04/03/cryptographic-right-answers.html)
  has been a useful page, but the cryptography world keeps moving!

