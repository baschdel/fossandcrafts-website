title: 8: Stefano Zacchiroli on preserving source code at Software Heritage
date: 2020-09-03 09:45
tags: episode, theatre, rpg, freeform univeral
slug: 8-stefano-zacchiroli-software-heritage
summary: Stefano Zacchiroli talks about Software Heritage's role as an archival institution for source code
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/008-stefano-zacchiroli-software-heritage.ogg" length:"26003830" duration:"00:42:12"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/008-stefano-zacchiroli-software-heritage.mp3" length:"40527403" duration:"00:42:12"
---
We are *extremely* excited to have on our first FOSS & Crafts guest:
[Stefano Zacchiroli](https://upsilon.cc/~zack/)!
(Also known on some corners of the FOSS world as just "zack".)
Stefano has a long history of FOSS advocacy, most famously for
[his role in Debian](https://upsilon.cc/~zack/hacking/debian/)
where he served three well-regarded terms as Debian Project Leader.
These days zack works on
[Software Heritage](https://www.softwareheritage.org/),
an archival institution for software source code.
We talk about how Software Heritage plays a role in common with other
[GLAM institutions](https://en.wikipedia.org/wiki/GLAM_(industry_sector))
(which stands for "Galleries, Libraries Archives and Museums").

. o O (Could we possibly have a more appropriate FOSS & Crafts first
guest episode?)

**Links:**

 - [Software Heritage](https://www.softwareheritage.org/)

   - [Information on Software Heritage for developers](https://www.softwareheritage.org/community/developers/)

   - [Support Software Heritage!](https://www.softwareheritage.org/support/)

 - [Inria](https://www.inria.fr/en)

 - [Debian](https://www.debian.org/)

 - [Guix](https://guix.gnu.org/)

 - [Software Heritage and GNU Guix join forces to enable long term reproducibility](https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/)

 - [Tragedy of the Commons](https://en.wikipedia.org/wiki/Tragedy_of_the_commons)

 - [Free-rider problem](https://en.wikipedia.org/wiki/Free-rider_problem)

 - [Rivalrous vs non-rivalrous goods](https://en.wikipedia.org/wiki/Rivalry_(economics))
