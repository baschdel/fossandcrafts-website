title: 28: FOSS Stitch w/ Elana Hashman and Katie McLaughlin
date: 2021-05-23 16:17
tags: foss, crafts, textiles, stitching, ih
slug: 28-foss-stitch-with-ehashman-glasnt
summary: Elana Hashman and Katie McLaughlin join us to talk about Ih, cross stitching, and foss meets textiles in general!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/028-foss-stitch-with-ehashman-glasnt.ogg" length:"44479828" duration:"01:13:48"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/028-foss-stitch-with-ehashman-glasnt.mp3" length:"70859879" duration:"01:13:48"
---
![Demo render of an alpaca embroidery pattern, with a rendered alpaca image](https://fossandcrafts.org/static/images/blog/ih-demo-render.png)

[Elana Hashman](https://hashman.ca/) (Python Software Foundation
Fellow and open source hacker) and [Katie McLaughlin](https://glasnt.com/)
(Python Software Foundation Fellow and crafter) join us to talk about
F(L)OSS meets embroidery and cross stitching (FOSS stitching?)
including a significant conversation about FLOSS vs embroidery
floss.

Much is also conversed about [ih](https://github.com/glasnt/ih), a
project started by Katie with contributions from Elana, a python
project which helps generate embroidery patterns from images.

**Links:**

 - [ih!](https://github.com/glasnt/ih)

   - ih presented at PyCon 2019: [Katie McLaughlin - A Right Stitch-up: Creating embroidery patterns with Pillow](https://www.youtube.com/watch?v=Fllch-WwzWM)

   - [ih, as a service](https://github.com/glasnt/ih-aas)

 - [DMC (Dollfus-Mieg et Compagnie)](https://en.wikipedia.org/wiki/DMC_(company)),
   the textile company mentioned several times

 - [Python Pillow](https://python-pillow.org/) (continuation of PIL,
   the Python Imaging Library)

 - [Linux Conf AU](https://linux.conf.au/), which has had a lot of
   "stitch and bitch" type events: [2018 art & tech miniconf](https://www.youtube.com/playlist?list=PLAObS6o1v0zKRBoEwPD6YreMfGFvh6zdM),
   [2019 knit, crochet, sew BoF](https://2019.linux.conf.au/wiki/Knit-crochet-sew-crafting_BoF),
   [2020 creative arts miniconf](https://lca2020.linux.org.au/programme/miniconfs/creative-arts/)

 - [Cross Stitch Carpentry](https://sgibson91.github.io/cross-stitch-carpentry/index.html)

 - [ginger coons' open color standard work](http://adaptstudio.ca/ocs/)

 - [freieFarbe](https://www.freiefarbe.de/en/) Open Color initiative, [appears to be CC BY 4.0](https://www.freiefarbe.de/en/verein/open-standards-open-content-neu/)
 
 - Stitching patterns [really are a kind of domain specific visual programming language](https://twitter.com/ehashdn/status/1310259859611242498) (see the response, ["What is this, assembler?"](https://twitter.com/fuzzychef/status/1314721786601201664))

 - [Core rope memory](https://en.wikipedia.org/wiki/Core_rope_memory) was usually [hand-woven by women](https://handwovenmagazine.com/weaving-history-core-rope-memory/) in early computing days ([more](https://www.fastcompany.com/90363966/the-guts-of-nasas-pioneering-apollo-computer-was-handwoven-like-a-quilt))

Still here?  How about some extra images?

Morgan's needlework of an alpaca, made with alpaca fiber:

![Needlework of an alpaca](https://fossandcrafts.org/static/images/blog/AlpacaNeedlework.jpg)

Kirby quilt that Morgan did for a babby:

![Needlework of an alpaca](https://fossandcrafts.org/static/images/blog/KirbyQuilt.jpg)
