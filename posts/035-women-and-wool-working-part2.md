title: 35: Women and Wool Working in the Ancient Roman Empire, Part 2
date: 2021-08-30 15:45
tags: crafts, dissertation, academia
slug: 35-women-and-wool-working-part2
summary: Morgan presents the second part of her PhD dissertation
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/035-women-and-wool-working-part2.ogg" length:"51460821" duration:"01:02:59"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/035-women-and-wool-working-part2.mp3" length:"60480350" duration:"01:02:59"
---
In [Part 1 of Women and Wool Working in the Ancient Roman
Empire](https://fossandcrafts.org/episodes/34-women-and-wool-working-part1.html),
we discussed the practical matters of textile production in domestic
and commercial contexts. In this second episode, we look at the
performative ways that textile production was used to construct
women's identities. This includes the incorporation of textile tools
and production into rites of passage such as marriage, childbirth, and
death as a symbol of the virtuous matron.  We further discuss
religious use and association of textile production through the
stories of the Fates, Arachne, and the Virgin Mary.  We then come
around to weave the rest of the narrative together: could the piece
that fits in the women-shaped hole of textile production in ancient
Rome be... women?

This episode is dedicated in loving memory of Laura Callahan-Hazard
and Sigrid Steinbock, both enthusiastic supporters of Morgan's
dissertation, themselves both textile artists, and who both had wanted
to read Morgan's dissertation but left this world too soon.

**Links:**

 - [Morgan's dissertation](https://mlemmer.org/dissertation/)

 - [Episode 34: Women and Wool Working in the Ancient Roman Empire, Part 1](https://fossandcrafts.org/episodes/34-women-and-wool-working-part1.html)

 - Trinkl, Elisabeth. 2004. ["Zum Wirkungskreis einer kleinasiatischen Matrona anhand ausgewählter Funde aus dem Hanghaus 2 in Ephesos."]([https://www.academia.edu/30067944/Zum_Wirkungskreis_einer_kleinasiatischen_matrona_anhand_ausgew%C3%A4hlter_Funde_aus_dem_Hanghaus_2_in_Ephesos) In *Jahreshefte des Österreichischen archäologischen Instituts in Wien.* 73:281-303

 - Roman version of the Arachne Myth by [Ovid, The Metamorphoses VI](https://www.poetryintranslation.com/PITBR/Latin/Metamorph6.php#anchor_Toc64106362) **Content Warning:** suicide, oblique mentions to rape, gods being jerks to mortals

 - Roman description of the three fates or Parcae by [Catullus, 64](https://diotima-doctafemina.org/translations/latin/catullus-64-the-wedding-of-peleus-and-thetis/), scroll down to line 305.

 - Roman version of the Europa Myth by [Ovid, The Metamorphoses, II, 833-875](https://www.poetryintranslation.com/PITBR/Latin/Metamorph2.php#anchor_Toc64106134) **Content Warning:** abduction, gods taking other forms to seduce women, gods being jerks to mortals

 - A summary of the mythology of [Leda and the Swan](https://www.greeklegendsandmyths.com/leda.html), very brief Roman summary in [Hyginus, Fabulae 77](https://topostext.org/work/206), scroll down to § 77. **Content Warning:** rape, gods taking other forms to seduce women, gods being jerks to mortals

 - Roman version of the Danae Myth by [Hyginus, Fabulae 63](https://topostext.org/work/206), scroll down to § 63. **Content Warning:** rape, gods taking other forms to seduce women, gods being jerks to mortals
