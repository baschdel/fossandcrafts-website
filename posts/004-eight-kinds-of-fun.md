title: 4: The Eight Kinds of Fun
date: 2020-08-06 11:00
tags: episode
slug: 4-the-eight-kinds-of-fun
summary: Looking at FOSS and Crafts through the lens(es) of the "Eight Kinds of Fun"
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/004-eight-kinds-of-fun.ogg" length:"39852353" duration:"00:58:33"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/004-eight-kinds-of-fun.mp3" length:"56223399" duration:"00:58:33"
---
Morgan and Chris look at the domains of FOSS and crafts
from the lens of the
[Eight Kinds of Fun](https://en.wikipedia.org/wiki/Marc_LeBlanc#8_Kinds_of_Fun),
traditionally used to analyze game design.
What kinds of ways do different people enjoy participating in creative
activities?
How can examining those help us understand how to grow our communities
to accomodate different participants with different styles of interests?

**Links:**

 - [MDA: A Formal Approach to Game Design and Game Research](http://www.cs.northwestern.edu/~hunicke/MDA.pdf)

 - [An article about the Eight Kinds of Fun, oriented at RPG GMs](https://theangrygm.com/gaming-for-fun-part-1-eight-kinds-of-fun/)

 - [Ludology episode about fun in board games](https://ludology.libsyn.com/ludology-episode-201-are-we-having-fun-yet), including the Eight Kinds of Fun

 - Chris's keynote from last year's ActivityPub conference:
   [ActivityPub: past, present future](https://conf.tube/videos/watch/2b9a985b-ccdd-49ce-a81b-ed00d2b47c85)
