title: 26: Dr. Morgan Lemmer-Webber, an academic journey
date: 2021-04-23 10:45
tags: academia
slug: 26-dr-mlemweb-academic-journey
summary: Morgan gets a PhD and we talk about that whoooooole long process
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/026-dr-mlemweb-academic-journey.ogg" length:"42686684" duration:"00:51:38"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/026-dr-mlemweb-academic-journey.mp3" length:"49582887" duration:"00:51:38"
---
Remember how we've been saying the entire run of this show "Morgan's
hard at work at finishing her PhD dissertation?"
Well guess what!
She finally got it handed in and defended it...
Morgan is now officially Dr. Morgan Lemmer-Webber!
(She still has to wrap up a few edits but hey it's official now!)

Morgan walks us through her experiences of the graduate school
process, from applying (and re-applying) to schools, to a masters
program, to a PhD program, and the many fun steps, bumps, and
adventures in-between.

Not much in terms of show notes this episode, but here are some
pictures!

![Chris hovering with an envelope to hopefully congratulate Morgan](/static/images/blog/pre-dr-mlemweb-envelope-scaled.jpg)

![Morgan's face after finally being called "Doctor Morgan Lemmer-Webber" by her committee](/static/images/blog/dr-mlemweb-scaled.jpg)

![The Roman two-beam loom](/static/images/blog/roman-loom-scaled.jpg)

![Morgan working with the Roman two-beam loom](/static/images/blog/working-with-roman-loom-scaled.jpg)

Congratulations again, Morgan!
