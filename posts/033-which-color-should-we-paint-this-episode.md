title: 33: Which Color Should We Paint This Episode?
date: 2021-08-01 17:50
tags: foss, bikeshedding
slug: 33-which-color-should-we-paint-this-episode
summary: We talk about the concept of "bikeshedding", as well as the email that popularized the term
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/033-which-color-should-we-paint-this-episode.ogg" length:"30845211" duration:"00:38:11"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/033-which-color-should-we-paint-this-episode.mp3" length:"36666273" duration:"00:38:11"
---
In this episode, we discuss "bikeshedding" (also known as the
[Law of Triviality](https://en.wikipedia.org/wiki/Law_of_triviality)),
the famous proposition that complex contributions and ideas (such as
plans to build a nuclear power plant), often of high impact and
importance, move forward with relatively little interference, whereas
simple contributions and conversations (such as which color to paint a
bikeshed) get caught up in committee and high-volume debate, and how
this tends to impact FOSS communities.
We do a (slightly dramatic) reading of the original email, hold a
conversation about it, and then come back to the topic with a twist
right after everyone (including ourselves) thought the episode was
over.

**Links:**

 - The original bikeshed email hosted at [shed.bike](https://shed.bike/)

 - But wait!  Use [bikeshed.org](https://bikeshed.org/) instead!

 - Wait!  You should link to the [white background page](https://white.bikeshed.org/)!
   No, the [green one](https://green.bikeshed.org/)!  No, [blue](https://blue.bikeshed.org/)!
   No, [purple](https://purple.bikeshed.org/)!

 - [Poul-Henning Kamp's page on the subject](http://phk.freebsd.dk/sagas/bikeshed/)

 - But wait!  [Brett Glass (and others) respond!](https://www.quora.com/Hacker-Culture-Who-was-Brett-Glass-named-in-the-original-bikeshed-email)

 - [yourlogicalfallacyis.com](https://yourlogicalfallacyis.com/),
   but be sure to read about
   [The Fallacy Fallacy](https://yourlogicalfallacyis.com/the-fallacy-fallacy)
   before you start linking to these to try to win an argument on the internet
