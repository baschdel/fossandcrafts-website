title: 31: Talking Pressbooks and OER with Steel Wagstaff
date: 2021-07-11 17:00
tags: foss, free culture, oer, publishing, pressbooks, open access
slug: 31-pressbooks-with-steel-wagstaff
summary: Steel Wagstaff joins us to talk about Pressbooks and Open Educational Resources
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/031-pressbooks-with-steel-wagstaff.ogg" length:"32270081" duration:"00:52:08"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/031-pressbooks-with-steel-wagstaff.mp3" length:"50057272" duration:"00:52:08"
---
[Steel Wagstaff](http://steelwagstaff.info/) joins us to talk about
their work at [Pressbooks](https://pressbooks.org), a FOSS based book
publishing suite particularly focused on
[Open Educational Resources](https://en.wikipedia.org/wiki/Open_educational_resources)
(OER), as well as talking about OER generally, open access, and
education as a fundamental human right!

 - [Pressbooks](https://pressbooks.org) ([git repos](https://github.com/pressbooks))
 - [Pressbooks Directory](https://pressbooks.directory/)
 - [Steel Wagstaff's website](http://steelwagstaff.info/), [Twitter account](https://twitter.com/steelwagstaff)
 - [5Rs of open content](https://opencontent.org/definition/)
 - [UN Sustainable development goals](https://sdgs.un.org/goals)
 - [Cape Town Declaration (September 2007)](https://www.capetowndeclaration.org/read-the-declaration + https://www.capetowndeclaration.org/cpt10/)
 - Charles Reznikoff, First There is the Need (Santa Barbara, California: Black Sparrow Press, 1977)
 - [Achieving the Dream](https://library.achievingthedream.org/)'s [Art History I](https://library.achievingthedream.org/herkimerarthistory1/), by Associate Professor Emeritus Bruce Schwabach of Herkimer Community College.  (CC BY!)
 - [Open Music Theory](https://viva.pressbooks.pub/openmusictheory/front-matter/introduction/) (which was also mentioned in [Episode 5: Milkytracker, chiptunes, and that intro music](https://fossandcrafts.org/episodes/5-milkytracker.html))  (CC BY-SA!)
 - [Fundamentals, Function, and Form](https://milnepublishing.geneseo.edu/fundamentals-function-form/)
 - [SMuFL: Standard Music Font Layout](https://www.smufl.org/fonts/)
 - [Inclusive Spectrums](https://ecampusontario.pressbooks.pub/incd2020exhibit/) ("This exhibition presents the preliminary major research project ideas of OCAD University’s Inclusive Design 2019/2021 cohort.")  (CC BY!)
 - [White Jaw](https://whitejaw.pressbooks.com/) by Laurel Bastian
 - [Rebus Foundation](https://rebus.foundation/)
 - [ScholarLed](https://scholarled.org/)
 - [Radical Open Access](https://radicalopenaccess.disruptivemedia.org.uk)
 - [Manifeold](https://manifoldapp.org/) (GPLv3!)
 - [editoria](https://editoria.pub/about-us/)
 - [Knowledge Futures Group](https://www.knowledgefutures.org/)
 - [PubPub](https://www.pubpub.org/)
 - [Scalar](https://scalar.me/anvc/scalar/)
