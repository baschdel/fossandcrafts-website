title: 50: The Spritely Institute
date: 2022-08-21 16:25
tags: foss, lisp, spritely
slug: 050-spritely-institute
summary: The Spritely Institute
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/050-the-spritely-institute.ogg" length:"00:28:15" duration:"24431227"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/050-the-spritely-institute.mp3" length:"00:28:15" duration:"27129679"
---
[![Spritely Institute's goblin character holding up the Spritely Institute flask](/static/images/blog/2022-08-12_The-Spritely-Institute_Mascot-nobg2-scaled.png)](https://spritely.institute)

The [Spritely Institute](https://spritely.institute/)
(of which Christine is CTO) just
[announced its multi-year grant](https://spritely.institute/news/ffdw-support-announcement.html)
by the [Filecoin Foundation for the Decentralized Web](https://www.ffdweb.org/)
and gave a [tour of its current tech](https://spritely.institute/news/blast-off-spritely-institutes-tech-tour.html)!
This is a big moment that's been in the works for a while, as
Spritely moves hands towards real stewardship by a
[real nonprofit](https://spritely.institute/news/spritely-institute-501c3-approval.html)!

Also also!  The video recording of the Lisp/Scheme workshop
(based on [A Scheme Primer](https://spritely.institute/static/papers/scheme-primer.html)) is released!
*Unlock Lisp / Scheme's magic: beginner to Scheme-written-in-Scheme in one hour!*
([PeerTube](https://share.tube/w/gdtnuipKbbVdR2u1murL4t), [YouTube](https://www.youtube.com/watch?v=DDROSL-gGOo), )

**Links:**

- [Spritely Networked Communities Institute](https://spritely.institute)

  - [FFDW funding announcement](https://spritely.institute/news/ffdw-support-announcement.html)

  - [Tech tour](https://spritely.institute/news/blast-off-spritely-institutes-tech-tour.html)

  - [Donate to the Spritely Institute](https://spritely.institute/donate/)!

- FOSS & Crafts episodes about Spritely:

  - The [What is Spritely](https://fossandcrafts.org/episodes/9-what-is-spritely.html)
    episode, where Morgan says "get in the car Christine you need to
    talk about your project", is the first time Christine laid out the
    broader (early) plans for Spritely in depth!  (In that sense,
    FOSS & Crafts has been here for much of Spritely's journey, as many
    of our listeners know!)

  - [Spritely Updates! (November 2021)](https://fossandcrafts.org/episodes/38-spritely-updates-november-2021.html)

  - Less directly,
    [Mark S. Miller on Distributed Objects, Part 1](https://fossandcrafts.org/episodes/46-mark-miller-on-distributed-objects-part-1.html)
    talks about much of the tech that informs Spritely's design!

  - [Spritely Institute's jobs page](https://spritely.institute/jobs/)
    which will have jobs posted on it like, real soon now

  - Spritely Institute is also the org that published
    [A Scheme Primer](https://spritely.institute/static/papers/scheme-primer.html),
    which we've talked about before

- [Free as in Freedom](http://faif.us) has talked about how the IRS
  has been more cautious about granting nonprofit status to FOSS orgs
  in [Episode 0x4E (IRS Refusal Redux)](http://faif.us/cast/2014/sep/23/0x4E/)
  
- Some background about Randy Farmer (Spritely Institute's Executive Director):

  - Randy co-founded [Lucasfilm's Habitat](https://en.wikipedia.org/wiki/Habitat_(video_game)),
    the world's first graphical massively multiplayer virtual world,
    which ran on the Commodore 64 in 1985 (!!!)

    - Revival over at [neohabitat.org](http://neohabitat.org)

    - See the hilarious
      [marketing video](https://www.youtube.com/watch?v=VVpulhO3jyc)

    - [The Lessons of Lucasfilms Habitat](https://web.stanford.edu/class/history34q/readings/Virtual_Worlds/LucasfilmHabitat.html)
      is one of the most cited papers about virtual community designs
      of all times, and still holds up today

  - Electric Communities Habitat was Habitat's followup.

    - Hard to find information on, but here's a
      [Randy demo'ing the system from 1997](https://www.youtube.com/watch?v=KNiePoNiyvE)!

    - The [E Programming Language](http://erights.org/),
      on which much of Spritely is designed, came from EC Habitat.
      See
      [Mark S. Miller on Distributed Objects, Part 1](https://fossandcrafts.org/episodes/46-mark-miller-on-distributed-objects-part-1.html)
      for more on that (and hey, when are we getting out part 2?)
    
  - Randy co-hosts a podcast called
    [Social Media Clarity](https://socialmediaclarity.libsyn.com/)
    which has some interesting episodes.

- See also Spritely Institute's brilliant engineer Jessica Tallon
  [writing about her experiences](https://spritelyproject.org/news/interview-with-jessica-tallon.html)
  and especially her
  [pebble bank design](https://spritelyproject.org/news/pebble-bank.html)!

