title: 21: Vicky Steeves on Reproducibility, Open Research, & Librarians (... and game modding)
date: 2021-01-17 22:40
tags: libraries, academia, open research, foss, crafts, games, mods
slug: 21-vicky-steeves
summary: An interview with Vicky Steeves about the intersection of FOSS, libraries, and open access... not to mention what FOSS can learn from game modding communities!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/021-vicky-steeves.ogg" length:"34733669" duration:"00:57:41"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/021-vicky-steeves.mp3" length:"55378593" duration:"00:57:41"
---
We're joined by [Vicky Steeves](https://vickysteeves.com/), a
hyper-talented librarian specializing in data management, open and
reproducible research, and the overlap between FOSS, free culture, and
library sciences!
We dive into all of that... plus a bit of crafting... and
even... what's this?
A discussion of what the FOSS world can learn from the world of game
modding (and vice versa)!

**Links:**

 - [ISAGE](https://investigating-archiving-git.gitlab.io/)
   (Investigating & Archiving the Scholarly Git Experience)
   and, to be a bit meta,
   [ISAGE's own data as open and reproducible research](https://investigating-archiving-git.gitlab.io/updates/iasge-data-published/)!

 - [Sherpa Romeo](https://v2.sherpa.ac.uk/romeo/)

 - [ReproZip](https://www.reprozip.org/)

 - Related F&C episode: [Stefano Zacchiroli on preserving source code at Software Heritage](https://fossandcrafts.org/episodes/8-stefano-zacchiroli-software-heritage.html)

 - [bwFLA - Emulation as a Service](http://eaas.uni-freiburg.de/)

 - [Stardew Valley modding page](https://stardewcommunitywiki.com/Modding:Player_Guide/Getting_Started) (note: Stardew Valley is not itself FOSS)

 - [Programming owes its strength to our long legacy of knitting](https://opensource.com/article/18/6/how-programming-evolved-knitting) by Carrie Stokes

 - [Project Jupyter](https://jupyter.org/)

 - [Git](https://git-scm.com/), [git-annex](https://git-annex.branchable.com/), and
   [git-lfs](https://git-lfs.github.com/)

 - [Docker](https://www.docker.com/)

 - [The Types, Roles, and Practices of Documentation in Data Analytics Open Source Software Libraries: A Collaborative Ethnography of Documentation Work](https://stuartgeiger.com/articles/2018-05-28-cscw-documentation/) by [R. Stuart Geiger](https://stuartgeiger.com/)

