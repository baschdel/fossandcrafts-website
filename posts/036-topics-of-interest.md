title: 36: Topics of interest!
date: 2021-09-11 17:22
tags: topics-of-interest, foss, crafts
slug: 36-topics-of-interest
summary: Lightning round!  Morgan and Christine blast through an array of topics they're currently interested in!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/036-topics-of-interest.ogg" length:"39925492" duration:"00:47:41"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/036-topics-of-interest.mp3" length:"45781948" duration:"00:47:41"
---
Lightning round!  Morgan and Christine blast through a bunch of
snack-sized topics they're currently interested in, ranging from an
actual FOSS video game made for the NES, to "Free Soft Wear" clothing,
to compiler towers!

![Free Soft Wear tag](https://mlemmer.org/images/IMG_20210821_120040scaled.jpg)

*above image from [Morgan's blogpost on "free soft wear"](https://mlemmer.org/blog/free_soft_wear/)*

**Links:**

 - This episode's title was inspired by Ian Bicking's 2009 PyCon talk,
   "Topics of Interest", but it's bitrotted off the internet so we
   can't link you to that one.  Boooooo!

 - [Nova the Squirel](https://novasquirrel.itch.io/nova-the-squirrel)
   by... [Nova the Squirrel](http://novasquirrel.com/)

   - [Source code!](https://github.com/NovaSquirrel/NovaTheSquirrel2)

   - [upcoming SNES sequel!](https://github.com/NovaSquirrel/NovaTheSquirrel2)

   - You might need an NES emulator... [Mednafen](https://mednafen.github.io/)
     is good and can play games on a lot of systems

 - [Mark Bitman's "How to Cook Everything Vegetarian" First edition](https://www.goodreads.com/book/show/1587362.How_to_Cook_Everything_Vegetarian)
   and no I'm not linking you to the second edition which is good but
   not as deeply instructive

 - [Elena "of Valhalla"'s blogpost about tie-on pockets](https://www.trueelena.org/clothing/projects/pair_of_pockets.html)

   - [see also](https://pourlavictoire.blogspot.com/2021/03/another-kind-of-18th-century-pocket.html)
   
   - [see also](https://www.vam.ac.uk/articles/make-your-own-pockets) ([see also](https://library.si.edu/digital-library/book/workwomansguide00hale))

 - [Episode 17: Gardening, from seedling to seasoned](https://fossandcrafts.org/episodes/17-gardening-seedling-to-seasoned.html)

 - [Guile](https://www.gnu.org/software/guile/)'s [compiler tower](https://www.gnu.org/software/guile/manual/html_node/Compiler-Tower.html)

 - [Kat Walsh's awesome necklace](https://twitter.com/mindspillage/status/1406330113998811137) she made during [Hack & Craft](https://fossandcrafts.org/hack-and-craft/)

 - [Jim's Big Ego](https://bigego.com/)'s song [Mix Tape](https://jimsbigego.bandcamp.com/track/mix-tape) from the album [They're Everywhere!](https://bigego.com/cds/f/Albums/345)

   - If you like this song, you might really like the album [free*](https://bigego.com/cds/f/Albums/1139) which Christine would argue tells a collective narrative very relevant to this podcast

 - [Robo Rally](https://boardgamegeek.com/boardgame/18/roborally)!
   Program robots!

 - [Morgan's blogpost on "free soft wear"](https://mlemmer.org/blog/free_soft_wear/) (term coined by the great [Kat Walsh](http://www.mindspillage.org/)!)

 - Morgan's [tablet weaving pattern for the "html strap"](/static/images/blog/modified-tablet-weaving-pattern-index-card.jpg) (Partly inspired by [this pattern](https://i.pinimg.com/736x/99/0e/ba/990ebaf1d1bbab441bf327efefee08ec.jpg), but honestly mostly different.  Bonus question for the reader: what constitutes a derivative work for weaving patterns?)

 - [Eat the Weeds](https://www.eattheweeds.com/) is a great resource to learn about what kinds of weeds you can eat, and which ones you can't, but even then, be careful.  Really, seriously, be careful.  [Poison hemlock](https://en.wikipedia.org/wiki/Conium_maculatum) looks almost just like its more innocent cousin [Queen Anne's lace](https://en.wikipedia.org/wiki/Daucus_carota) except that it will kill you quickly and painfully.  There's lots of great wild stuff out there, but be careful, and maybe take a class or instructions or help from someone who knows what they're talking about, or only eat the stuff that's generally agreed upon as "there's nothing dangerous you could mistake this for".  And even then, be sure!
