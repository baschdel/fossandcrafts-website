title: 32: Happy FOSS & Crafts anniversary!
date: 2021-07-20 17:45
tags: meta
slug: 32-happy-fossandcrafts-anniversary
summary: Chris and Morgan reflect on one year of FOSS & Crafts, and announce FOSS & Crafts Studios
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/032-happy-fossandcrafts-anniversary.ogg" length:"28794029" duration:"00:33:16"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/032-happy-fossandcrafts-anniversary.mp3" length:"31942071" duration:"00:33:16"
---
Chris and Morgan reflect on one year of FOSS & Crafts, as well as
announcing... FOSS & Crafts Studios!

**Links:**

 - [FOSS & Crafts Studios on Patreon](https://www.patreon.com/fossandcrafts)
   (still also the right place to support Chris Lemmer-Webber's FOSS work)

 - Thank you to our guests: [Nick and LP](https://fossandcrafts.org/episodes/6-demonic-zoooo-part-1.html), [Zack](https://fossandcrafts.org/episodes/8-stefano-zacchiroli-software-heritage.html), [Kate and Frankie](https://fossandcrafts.org/episodes/10-what-goblins-ch1-what-are-goblins.html), [Sebastian](https://fossandcrafts.org/episodes/13-apconf-2020.html), [Bassam](https://fossandcrafts.org/episodes/16-bassam-kurdali-blender-open-movies-education.html), [Tristan](https://fossandcrafts.org/episodes/17-gardening-seedling-to-seasoned.html), [Sumana](https://fossandcrafts.org/episodes/18-sumana-harihareswara.html), [Mallory](https://fossandcrafts.org/episodes/19-mallory-knodel.html), [Vicky](https://fossandcrafts.org/episodes/21-vicky-steeves.html), [Steve](https://fossandcrafts.org/episodes/23-nerdout-fuzzy-and-crisp.html), [Elana and Katie](https://fossandcrafts.org/episodes/28-foss-stitch-with-ehashman-glasnt.html), and [Steel](https://fossandcrafts.org/episodes/31-pressbooks-with-steel-wagstaff.html).

 - [3: Textile production and a nostalgic past](https://fossandcrafts.org/episodes/3-textile-production-and-a-nostalgic-past.html)

 - [28: FOSS Stitch w/ Elana Hashman and Katie McLaughlin](https://fossandcrafts.org/episodes/28-foss-stitch-with-ehashman-glasnt.html)

 - [5: Milkytracker, chiptunes, and that intro music](https://fossandcrafts.org/episodes/5-milkytracker.html)

 - [22: Crafting the past&#x2026; or trying to](https://fossandcrafts.org/episodes/22-crafting-the-past.html)

 - [9: What is Spritely?](https://fossandcrafts.org/episodes/9-what-is-spritely.html)

 - [11: An Ethics of Agency](https://fossandcrafts.org/episodes/11-an-ethics-of-agency.html)

 - [20: Hygiene for a computing pandemic](https://fossandcrafts.org/episodes/20-hygiene-for-a-computing-pandemic.html)

 - [26: Dr. Morgan Lemmer-Webber, an academic journey](https://fossandcrafts.org/episodes/26-dr-mlemweb-academic-journey.html)

 - [1: Collaborative Storytelling with Dice](https://fossandcrafts.org/episodes/1-collaborative-storytelling-with-dice.html)

 - [6: What Escaped from the Demonic Z.O.O.O.O. (part 1)](https://fossandcrafts.org/episodes/6-demonic-zoooo-part-1.html) and [(part 2)](https://fossandcrafts.org/episodes/7-demonic-zoooo-part-2.html)

 - [10: The What Goblins Saga, Chapter 1: What Are Goblins?](https://fossandcrafts.org/episodes/10-what-goblins-ch1-what-are-goblins.html) and [Chapter 2: Trees, Friends, and Static](https://fossandcrafts.org/episodes/12-what-goblins-ch2-trees-friends-static.html)

 - [25: Governance, Leadership, and Founder's Syndrome](https://fossandcrafts.org/episodes/25-governance-leadership-founders-syndrome.html)

 - [30: Gender and Sexuality, A Personal Perspective](https://fossandcrafts.org/episodes/30-gender-sexuality-personal-perspective.html)

 - [23: Nerdout! Fuzzy and crisp systems](https://fossandcrafts.org/episodes/23-nerdout-fuzzy-and-crisp.html) and [27: Nerdout! Game Design and Social Systems](https://fossandcrafts.org/episodes/27-nerdout-game-design-social-systems.html)

 - Shrini's toots about the Hack and Craft:[Post1](https://mastodon.social/@tshrinivasan/106519733756987260) [Post 2](https://mastodon.social/@tshrinivasan/106519922297555005) \*We did extrapolate a lot more than the posts say ;)
