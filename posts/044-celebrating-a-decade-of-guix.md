title: 44: Celebrating a Decade of Guix
date: 2022-04-30 16:25
tags: foss, guix, distros, package management, lisp, scheme
slug: 44-celebrating-a-decade-of-guix
summary: We celebrate ten years of Guix by highlighting ten great things about Guix!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/044-celebrating-a-decade-of-guix.ogg" length:"00:37:27" duration:"32071366"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/044-celebrating-a-decade-of-guix.mp3" length:"00:37:27" duration:"35960745"
---
[Guix](https://guix.gnu.org/) turns ten!
We celebrate [Guix's first decade](https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/)
by highlighting ten great things about Guix!
Hear all about functional package management, time-traveling operating systems,
and why "Composable DSLs" are great!

**Links:**

 - [Guix](https://guix.gnu.org/)

 - [Stories about 10 years of Guix, from the Guix blog](https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/)

 - [Nix](https://nixos.org/)

 - Cool Guix features highlighted in this episode:

   - [Grafts (for security updates)](https://guix.gnu.org/manual/en/html_node/Security-Updates.html)

   - [guix challenge](https://guix.gnu.org/manual/en/html_node/Invoking-guix-challenge.html)

   - [guix shell](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html) and [guix environment](https://guix.gnu.org/manual/en/html_node/Invoking-guix-environment.html)

   - [guix pack](https://guix.gnu.org/manual/en/html_node/Invoking-guix-pack.html)

 - [Nonguix](https://gitlab.com/nonguix/nonguix) (Proprietary!  Nonfree!  But sometimes some users need these things to get their computers to work...)

 - [Reproducible Builds](https://reproducible-builds.org/)

 - [Bootstrappable Builds](http://bootstrappable.org/)

 - [Mes](https://www.gnu.org/software/mes/) (see [this video for an introduction](https://archive.fosdem.org/2019/schedule/event/gnumes/))

 - [Reflections on Trusting Trust](https://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf) (aka the "Thompson Attack" described in the episode)

 - [virtualenv](https://virtualenv.pypa.io/en/latest/)
