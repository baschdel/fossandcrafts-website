title: 6: [Theatre] What Escaped from the Demonic Z.O.O.O.O. (part 1)
date: 2020-08-20 13:20
tags: episode, theatre, rpg, freeform univeral
slug: 6-demonic-zoooo-part-1
summary: Live narrative RPG recording telling the story of several demonic office workers forced to deal with the escape of a dangerous kind of creature
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/006-what-escaped-from-demonic-zoooo-pt1.ogg" length:"58215884" duration:"01:34:52"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/006-what-escaped-from-demonic-zoooo-pt1.mp3" length:"91087907" duration:"01:34:52"
---
On the first ever episode of FOSS and Crafts Theatre (a new subshow of
FOSS and Crafts), Chris and Morgan are joined by veteran role playing
game players Nick and LP to bring everyone a story of three demonic
employees of the international conglomerate, Demonstrative Industries.
Something has escaped from the Demonic Z.O.O.O.O. to the human realm,
and its up to our demon heroes to clean up the mess before anyone
finds out.  Can they succeed in their mission and keep their corporate
overlords pleased with them, or will things get dramatically out of
hand... or perhaps something in-between?  Find out on today's episode
(part one of two)!

**Links:**

 - If you haven't already, perhaps listen to
   [FOSS & Crafts Episode 1: Collabortive Storytelling with Dice](https://fossandcrafts.org/episodes/1-collaborative-storytelling-with-dice.html)
   which introduces the idea of narrative RPGs.
   (That's also where we floated the idea to listeners of doing live RPG
   episodes as a way of generating new free culture content, to which we
   got an enthusiastic response, leading to this new sub-show!)

 - See also [Freeform Universal](http://freeformuniversal.com/) (the
   RPG system used for this episode, explained in depth in Episode 1)!
