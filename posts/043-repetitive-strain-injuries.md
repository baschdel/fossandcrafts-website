title: 43: Repetitive Strain Injuries
date: 2022-03-31 16:30
tags: foss, crafts, sewing, rsi, free soft wear
slug: 43-repetitive-strain-injuries
summary: Repetitive strain injuries, preventing and dealing with them
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/043-repetitive-strain-injuries.ogg" length:"01:07:55" duration:"58242949"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/043-repetitive-strain-injuries.mp3" length:"01:07:55" duration:"65707734"
---
This week we’re talking about Repetitive Strain Injuries
(RSI). Christine and Morgan tell their stories bout over-using their
wrists from programming (prodded along by an injury) and writing
academic papers respectively. We discuss what you can do to treat or
minimize the effects of these injuries then cap it off with a
discussion of RSI gloves including Morgan's
[Free Soft Wear RSI glove pattern](https://mlemmer.org/blog/RSIgloves/).

 - [Repetitive Strain Injuries](https://en.wikipedia.org/wiki/Repetitive_strain_injury)
 - [Morgan's RSI gloves article](https://mlemmer.org/blog/RSIgloves/)
 - [Your Wrists Hurt, You Must Be a Programmer](https://gmarceau.qc.ca/articles/your-wrists-hurt-you-must-be-a-programmer.html)
 - [It's Not Carpal Tunnel Syndrome book](https://books.google.com/books/about/It_s_Not_Carpal_Tunnel_Syndrome.html?id=1GdEiu7JfUsC) (there are probably better resources out there now, this is what Christine read a decade ago)
 - [Workrave](https://workrave.org/)
 - [Some RSI exercises](https://web.archive.org/web/20040301074131/http://www.will-harris.com/yoga/rsi_excercises.html) that Christine thought were effective (old, but archived on internet archive... Christine still uses them sometimes)
