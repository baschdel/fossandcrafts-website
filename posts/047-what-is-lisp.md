title: 47: What is Lisp?
date: 2022-06-23 12:25
tags: foss, lisp
slug: 47-what-is-lisp
summary: An introduction to the ideas behind the Lisp family of languages
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/047-what-is-lisp.ogg" length:"00:29:44" duration:"25547089"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/047-what-is-lisp.mp3" length:"00:29:44" duration:"28557419"
---
This episode is all about the
[Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language))
family of programming languages!
Ever looked at Lisp and wondered why so many programmers gush about
such a weird looking programming language style?
What's with all those parentheses?
Surely there must be something you get out of them for so many
programming nerds to gush about the language!
We do a light dive into Lisp's history, talk about what makes Lisp
so powerful, and nerd out about the many, many kinds of Lisps out
there!

**Announcement:** Christine is gonna give an intro-to-Scheme tutorial
at our next [Hack & Craft](https://fossandcrafts.org/hack-and-craft/)!
Saturday July 2nd, 2022 at 20:00-22:00 ET!
Come and learn some Scheme with us!

**Links**:

 - Various histories of Lisp:
   - [History of Lisp](http://jmc.stanford.edu/articles/lisp.html) by John McCarthy
   - [The Evolution of Lisp](https://www.dreamsongs.com/Files/Hopl2.pdf) by Guy L. Steele and Richard P. Gabriel
   - [History of LISP](https://www.softwarepreservation.org/projects/LISP/) by Paul McJones

 - William Byrd's
   [The Most Beautiful Program Ever Written](https://www.youtube.com/watch?v=OyfBQmvr2Hc)
   demonstrates just how easy it is to write lisp in lisp, showing off the
   kernel of evaluation living at every modern programming language!

 - [M-expressions](https://en.wikipedia.org/wiki/M-expression) (the original math-notation-vision for users to operate on) vs [S-expressions](https://en.wikipedia.org/wiki/S-expression) (the structure Lisp evaluators actually operate at, in direct representational mirror of the typically, but not necessarily, parenthesized representation of the same).

 - Lisp-1 vs Lisp-2... well, rather than give a simple link and analysis,
   have [a thorough one](http://www.nhplace.com/kent/Papers/Technical-Issues.html).

 - [Lisp machines](https://en.wikipedia.org/wiki/Lisp_machine)

   - MIT's [CADR](https://dspace.mit.edu/handle/1721.1/5718) was the
     second iteration of the lisp machine, and the most influential
     on everything to come.  Then everything split when two separate
     companies implemented it...

   - [Lisp Machines, Incorporated (LMI)](https://en.wikipedia.org/wiki/Lisp_Machines),
     founded by famous hacker Richard Greenblatt, who aimed to keep
     the MIT AI Lab hacker culture alive by only hiring programmers
     part-time.
     
   - [Symbolics](https://en.wikipedia.org/wiki/Symbolics) was the
     other rival company.  Took venture capital money, was a
     commercial success for quite a while.
     
   - These systems were very interesting, there's more to them than
     just the rivalry.  But regarding that, the book
     [Hackers](https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution)
     (despite its [issues](http://opentranscripts.org/transcript/programming-forgetting-new-hacker-ethic/))
     captures quite a bit about the AI lab before this and then its
     split, including a ton of Lisp history.

   - Some interesting things happening over at [lisp-machine.org](https://lisp-machine.org/)

 - The [GNU manifestio](https://www.gnu.org/gnu/manifesto.en.html)
   mentions Lisp quite a bit, including that the plan was for the
   system to be mostly C and Lisp.

 - [Worse is Better](https://www.dreamsongs.com/WorseIsBetter.html),
   including [the original](https://www.dreamsongs.com/RiseOfWorseIsBetter.html)
   (but the first of those two links provides a lot of context)

 - The [AI winter](https://en.wikipedia.org/wiki/AI_winter).
   Bundle up, lispers!

 - Symbolics' [Mac Ivory](https://en.wikipedia.org/wiki/Symbolics#Ivory_and_Open_Genera)

 - [RISC-V tagged architecture](https://www.ndss-symposium.org/wp-content/uploads/2019/02/ndss2019_10-3_Weiser_paper.pdf), plus this [lowRISC tagged memory tutorial](https://lowrisc.org/docs/tagged-memory-v0.1/).  (We haven't read these yet, but they're on the reading queue!)

 - [Scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language))
   - There's a lot of these... we recommend [Guile](https://www.gnu.org/software/guile/)
     if you're interested in using Emacs (along with [Geiser](https://www.nongnu.org/geiser/)), and [Racket](https://racket-lang.org/) if you're looking for a more gentle introduction (DrRacket, which ships with Racket, is a friendly introduction)
   - The [R5RS](https://schemers.org/Documents/Standards/R5RS/) and [R7RS-small](https://small.r7rs.org/) specs are very short and easy to read especially
   - See [this section of the Guile manual](https://www.gnu.org/software/guile/manual/html_node/Guile-and-Scheme.html) for a bit of... history

 - [Common Lisp](https://en.wikipedia.org/wiki/Common_Lisp)...
   which, yeah there are multiple implementations, but these days
   really means [SBCL](http://www.sbcl.org/) with
   [Sly](https://github.com/joaotavora/sly) or [SLIME](https://slime.common-lisp.dev/)

 - [Clojure](https://clojure.org/) introduced functional
   datastructures to the masses (okay, maybe not the masses).  Neat
   stuff, though not a great license choice (even if technically FOSS)
   in our opinion and Rich Hickey kinda
   [blew up his community](https://gist.github.com/richhickey/1563cddea1002958f96e7ba9519972d9)
   so maybe use something else these days.

 - [Hy](https://docs.hylang.org/en/alpha/), always hy-larious

 - [Fennel](https://fennel-lang.org/), cutest lil' Lua Lisp you've ever seen

 - [Webassembly](https://webassembly.org/)'s text syntax isn't
   technically a Lisp, but let's be honest... is it technically
   *not* a Lisp either?!

 - [Typed Racket](https://docs.racket-lang.org/ts-guide/) and
   [Hackett](https://lexi-lambda.github.io/hackett/)

 - [Emacs](https://www.gnu.org/software/emacs/)... Lisp?... well let's just give you [the tutorial](https://www.gnu.org/software/emacs/manual/html_node/eintr/)!
   The [dreams of the 60s-80s are alive](https://www.youtube.com/watch?v=U4hShMEk1Ew) in Emacs.

 - [The Many Faces of an Undying Programming Language](https://jakob.space/blog/thoughts-on-lisps.html)
   is a nice little tour of some well known Lisps.

 - Actually, we
   [just did an episode about Emacs](https://fossandcrafts.org/episodes/41-learning-emacs.html),
   didn't we?

 - [Digital Humanities Workshops](https://fossandcrafts.org/episodes/14-digital-humanities-workshops.html) episode

 - We guess if you wanted to use Racket and VS Code, you could use [Magic Racket](https://marketplace.visualstudio.com/items?itemName=evzen-wybitul.magic-racket)?!
   We dunno, we've never used VS Code!  (Are we out of touch?!)

 - What about for Guile?!  Someone put some energy into
   [Guile Studio](https://elephly.net/guile-studio/)!

 - [Hack & Craft!](https://fossandcrafts.org/hack-and-craft/)
